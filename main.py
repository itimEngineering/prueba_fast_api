from fastapi import FastAPI, Request, HTTPException
from utils.jwt_manager import validate_token
from fastapi.security import HTTPBearer
from config.database import engine, Base
from fastapi.encoders import jsonable_encoder
from middlewares.error_handler import ErrorHandler


from routers.movie import movie_router
from routers.user import user_router

Base.metadata.create_all(bind=engine)

app = FastAPI()
app.title = 'API Boilerplate Fastapi'
app.description = 'Description API Boilerplate Fastapi'
app.version = '1'
app.contact = {
    'name': 'Sergio Anaya',
    'email': 'gerencia@itim-engineering.com'
}

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)


#devolver el token
class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data['email'] != "admin@gmail.com":
            raise HTTPException(status_code=403, detail="Credenciales son inválidas")



movies = [
    {
        'id': 1,
        'title': 'Avatar',
        'overview': 'En un exuberante planeta llamado pandora',
        'year': '2009',
        'rating': 7.8,
        'category': 'Acción'
    },
        {
        'id': 2,
        'title': 'Avatar',
        'overview': 'En un exuberante planeta llamado pandora',
        'year': '2009',
        'rating': 7.8,
        'category': 'Acción'
    }
]


@movie_router.get('/', tags=['home'])
def message():
    return {"Hello":"world"}
##return HTMLResponse('<h1>Hello World</h1>')




    

